`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:38:55 04/12/2016 
// Design Name: 
// Module Name:    Clock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Clock(
input Clk_100M,
output [3:0]SegmentDrivers,
output [7:0]SevenSegment


    );
reg [29:0]Count;
reg [29:0]seconds;
reg [29:0]minutes_unit;
reg [29:0]tens_unit;
always @(posedge Clk_100M) Count <= Count + 1'b1;



//assign SegmentDrivers = Count [29:16];
//assign SevenSegment = Count [25:18];
wire Reset; // Define a local wire to carry the Reset signal
Delay_Reset Delay_Reset1(Clk_100M, BTNS, Reset);
SS_Driver SS_Driver1(
Clk_100M, Reset,
4'd5, 4'd2, 4'd3, 4'd4, // Use temporary test values
SegmentDrivers, SevenSegment
);

endmodule

