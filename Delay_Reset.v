`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:49:59 04/12/2016 
// Design Name: 
// Module Name:    Delay_Reset 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Delay_Reset(
input Clk,
// Input clock signal
input Button,
// Input reset signal (external button)
output reg Reset
// Output reset signal (delayed)
);
//------------------------------------------------------------------------------
reg LocalReset;
reg [22:0]Count;
// Assume Count is null on FPGA configuration
//------------------------------------------------------------------------------
always @(posedge Clk) begin
LocalReset <= Button;
// Activates every clock edge
// Localise the reset signal
if(LocalReset) begin
Count <= 0;
Reset <= 1'b1; // Reset block
// Reset Count to 0
// Reset the output to 1
end else if(&Count) begin
Reset <= 1'b0;
// Count remains unchanged // When Count is all ones...
// Release the output signal
// And do nothing else
end else begin
// Otherwise...
Reset <= 1'b1;
// Make sure the output signal is high
Count <= Count + 1'b1;
// And increment Count
end
end
//------------------------------------------------------------------------------
endmodule